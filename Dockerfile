FROM cloudron/base:4.0.0@sha256:31b195ed0662bdb06a6e8a5ddbedb6f191ce92e8bee04c03fb02dd4e9d0286df

FROM php:8.0-fpm-bullseye as pimcore_php_fpm

RUN set -eux; \
    DPKG_ARCH="$(dpkg --print-architecture)"; \
    apt-get update; \
    apt-get install -y lsb-release; \
    echo "deb http://deb.debian.org/debian $(lsb_release -sc)-backports main" > /etc/apt/sources.list.d/backports.list; \
    apt-get update; \
    apt-get install -y --no-install-recommends \
        autoconf automake libtool nasm make pkg-config libz-dev build-essential openssl g++ \
        zlib1g-dev libicu-dev libbz2-dev zopfli libc-client-dev default-jre \
        libkrb5-dev libxml2-dev libxslt1.1 libxslt1-dev locales locales-all \
        ffmpeg html2text ghostscript libreoffice pngcrush jpegoptim exiftool poppler-utils git wget \
        libx11-dev python3-pip opencv-data facedetect webp graphviz cmake ninja-build unzip cron \
        liblcms2-dev liblqr-1-0-dev libjpeg-turbo-progs libopenjp2-7-dev libtiff-dev \
        libfontconfig1-dev libfftw3-dev libltdl-dev liblzma-dev libopenexr-dev \
        libwmf-dev libdjvulibre-dev libpango1.0-dev libxext-dev libxt-dev librsvg2-dev libzip-dev \
        libpng-dev libfreetype6-dev libjpeg-dev libxpm-dev libwebp-dev libjpeg62-turbo-dev \
        xfonts-75dpi xfonts-base libjpeg62-turbo \
        libonig-dev optipng pngquant inkscape; \
    \
    apt-get install -y libavif-dev libheif-dev optipng pngquant chromium chromium-sandbox; \
    docker-php-ext-configure pcntl --enable-pcntl; \
    docker-php-ext-install pcntl intl mbstring mysqli bcmath bz2 soap xsl pdo pdo_mysql fileinfo exif zip opcache sockets; \
    \
    wget https://imagemagick.org/archive/ImageMagick.tar.gz; \
        tar -xvf ImageMagick.tar.gz; \
        cd ImageMagick-7.*; \
        ./configure; \
        make --jobs=$(nproc); \
        make V=0; \
        make install; \
        cd ..; \
        rm -rf ImageMagick*; \
    \
    docker-php-ext-configure gd -enable-gd --with-freetype --with-jpeg --with-webp; \
    docker-php-ext-install gd; \
    pecl install -f xmlrpc imagick apcu redis; \
    docker-php-ext-enable redis imagick apcu; \
    docker-php-ext-configure imap --with-kerberos --with-imap-ssl; \
    docker-php-ext-install imap; \
    docker-php-ext-enable imap; \
    ldconfig /usr/local/lib; \
    \
    cd /tmp; \
    \
    wget -O wkhtmltox.deb https://github.com/wkhtmltopdf/packaging/releases/download/0.12.6-1/wkhtmltox_0.12.6-1.buster_${DPKG_ARCH}.deb; \
        dpkg -i wkhtmltox.deb; \
        rm wkhtmltox.deb; \
    \
    apt-get autoremove -y; \
        apt-get remove -y autoconf automake libtool nasm make cmake ninja-build pkg-config libz-dev build-essential g++; \
        apt-get clean; \
        rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* /usr/share/doc/* ~/.composer || true; \
    sync;

RUN echo "upload_max_filesize = 100M" >> /usr/local/etc/php/conf.d/20-pimcore.ini; \
    echo "memory_limit = 256M" >> /usr/local/etc/php/conf.d/20-pimcore.ini; \
    echo "post_max_size = 100M" >> /usr/local/etc/php/conf.d/20-pimcore.ini

ENV COMPOSER_ALLOW_SUPERUSER 1
ENV COMPOSER_MEMORY_LIMIT -1
COPY --from=composer/composer:2-bin /composer /usr/bin/composer

WORKDIR /var/www/html

CMD ["php-fpm"]

FROM pimcore_php_fpm as pimcore_php_debug

RUN apt-get update; \
    apt-get install -y --no-install-recommends \
      autoconf automake libtool nasm make pkg-config libz-dev build-essential g++ iproute2; \
    pecl install xdebug; \
    docker-php-ext-enable xdebug; \
    apt-get autoremove -y; \
    apt-get remove -y autoconf automake libtool nasm make pkg-config libz-dev build-essential g++; \
    apt-get clean; \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* /usr/share/doc/* ~/.composer || true

# allow container to run as custom user, this won't work otherwise because config is changed in entrypoint.sh
RUN chmod -R 0777 /usr/local/etc/php/conf.d

ENV PHP_IDE_CONFIG serverName=localhost

COPY files/entrypoint.sh /usr/local/bin
RUN chmod +x /usr/local/bin/entrypoint.sh

ENTRYPOINT ["/usr/local/bin/entrypoint.sh"]
CMD ["php-fpm"]

FROM pimcore_php_fpm as pimcore_php_supervisord

RUN apt-get update && apt-get install -y supervisor cron
COPY files/supervisord.conf /etc/supervisor/supervisord.conf

RUN chmod gu+rw /var/run
RUN chmod gu+s /usr/sbin/cron

ENV PIMCORE_VERSION=10.2.3

RUN mkdir -p /app/code/ && \
    COMPOSER_MEMORY_LIMIT=-1 composer create-project pimcore/skeleton=${PIMCORE_VERSION} /app/code/

RUN apt-get install -y --no-install-recommends nginx default-mysql-client redis && \
    rm -rf /etc/nginx/sites-enabled/default
COPY docker/ /

WORKDIR /app/code

RUN mv /app/code/config /app/code/config_default && \
    ln -s /app/data/pimcore/config /app/code/config && \
    mv /app/code/var /app/code/var_default && \
    ln -s /app/data/pimcore/var /app/code/var && \
    rm -rf /var/log/nginx/error.log /var/log/nginx/access.log && \
    ln -s /dev/stdout /var/log/nginx/error.log && \
    ln -s /dev/stdout /var/log/nginx/access.log && \
    rm -rf /var/lib/nginx && \
    ln -s /app/data/nginx/lib/ /var/lib/nginx && \
    ln -s /dev/stdout /usr/local/var/log/php-fpm.log && \
    mv /usr/local/etc/php/conf.d/20-pimcore.ini /usr/local/etc/php/conf.d/20-pimcore.ini.default && \
    ln -s /app/data/php/20-pimcore.ini /usr/local/etc/php/conf.d/20-pimcore.ini && \
    ln -s /app/data/pimcore/public/var /app/code/public/var

RUN chown -R www-data:www-data /app/code

ENV YQ_VERSION=v4.32.1 \
    YQ_BINARY=yq_linux_amd64 

RUN wget https://github.com/mikefarah/yq/releases/download/${YQ_VERSION}/${YQ_BINARY}.tar.gz -O - |\
  tar xz && mv ${YQ_BINARY} /usr/bin/yq

CMD ["/app/code/pimcore_start.sh"]
