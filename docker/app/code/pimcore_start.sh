#!/bin/bash

echo '=> Checking if this is the first run...'

if [[ ! -f /app/data/.setup.lock ]]; then
    echo 'No .setup.lock deteceting, running initial setup'
    echo '=> Copy default dirs'
    mkdir -p /app/data/pimcore

    if [[ ! -d /app/data/pimcore/config ]]; then
        cp -rv /app/code/config_default /app/data/pimcore/config
    fi

    if [[ ! -d /app/data/pimcore/var ]]; then
        cp -rv /app/code/var_default /app/data/pimcore/var
    fi

    if [[ ! -h /app/data/pimcore/ ]]; then
        echo '=> Symlink Source'
        ln -s /app/code/src/ /app/data/pimcore/
    fi

    echo '=> Running initial install'
    cd /app/code || exit
    ./vendor/bin/pimcore-install --admin-username=admin --admin-password=changeme123 \
    --mysql-username="$CLOUDRON_MYSQL_USERNAME" --mysql-password="$CLOUDRON_MYSQL_PASSWORD" --mysql-database="$CLOUDRON_MYSQL_DATABASE" \
    --mysql-host-socket="$CLOUDRON_MYSQL_HOST" --mysql-port="$CLOUDRON_MYSQL_PORT" \
    --mysql-host-socket="$CLOUDRON_MYSQL_HOST":"$CLOUDRON_MYSQL_PORT" \
    --no-interaction
    touch /app/data/.setup.lock
    chown -R www-data:www-data /app/data/pimcore/
fi

echo '=> Ensure NGINX lib folder is present'
if [[ ! -d /app/data/nginx/lib ]]; then
    echo '/app/data/nginx/lib missing . . . creating'
    mkdir -p /app/data/nginx/lib/
    chown -R www-data:www-data /app/data/nginx
fi

echo '=> Ensure pimcore php config exists'
if [[ ! -f /app/data/php/20-pimcore.ini ]]; then
    echo '=> Setup php config file'
    mkdir -p /app/data/php/
    cp /usr/local/etc/php/conf.d/20-pimcore.ini.default /app/data/php/20-pimcore.ini
fi

echo '=> Ensure pimcore public var folder exists'
if [[ ! -d /app/data/pimcore/public/var ]]; then
    mkdir -p /app/data/pimcore/public/var
fi

echo '=> Configure'

echo '=> Configure pimcore SMTP settings'
if [[ $CLOUDRON_MAIL_FROM_DISPLAY_NAME != "" ]]; then
    yq -i '.pimcore.email.sender.name = strenv(CLOUDRON_MAIL_FROM_DISPLAY_NAME)' /app/data/pimcore/config/config.yaml
    yq -i '.pimcore.email.return.name = strenv(CLOUDRON_MAIL_FROM_DISPLAY_NAME)' /app/data/pimcore/config/config.yaml
else
    yq -i '.pimcore.email.sender.name = strenv(CLOUDRON_MAIL_SMTP_USERNAME)' /app/data/pimcore/config/config.yaml
    yq -i '.pimcore.email.return.name = strenv(CLOUDRON_MAIL_SMTP_USERNAME)' /app/data/pimcore/config/config.yaml
fi
yq -i '.pimcore.email.sender.email = strenv(CLOUDRON_MAIL_SMTP_USERNAME)' /app/data/pimcore/config/config.yaml
yq -i '.pimcore.email.return.email = strenv(CLOUDRON_MAIL_SMTP_USERNAME)' /app/data/pimcore/config/config.yaml

mail_url="smtp://${CLOUDRON_MAIL_SMTP_USERNAME}:${CLOUDRON_MAIL_SMTP_PASSWORD}@${CLOUDRON_MAIL_SMTP_SERVER}:${CLOUDRON_MAIL_SMTP_PORT}" yq -i '.framework.mailer.transports.main = strenv(mail_url)' /app/data/pimcore/config/config.yaml
mail_url="smtp://${CLOUDRON_MAIL_SMTP_USERNAME}:${CLOUDRON_MAIL_SMTP_PASSWORD}@${CLOUDRON_MAIL_SMTP_SERVER}:${CLOUDRON_MAIL_SMTP_PORT}" yq -i '.framework.mailer.transports.pimcore_newsletter = strenv(mail_url)' /app/data/pimcore/config/config.yaml

echo '=> Setup pimcore Redis caching'
yq -i '.framework.cache.pools."pimcore.cache.pool".public = true' /app/data/pimcore/config/config.yaml
yq -i '.framework.cache.pools."pimcore.cache.pool".tags = true' /app/data/pimcore/config/config.yaml
yq -i '.framework.cache.pools."pimcore.cache.pool".default_lifetime = 31536000' /app/data/pimcore/config/config.yaml
yq -i '.framework.cache.pools."pimcore.cache.pool".adapter = "pimcore.cache.adapter.redis_tag_aware"' /app/data/pimcore/config/config.yaml
rc="${CLOUDRON_REDIS_URL}" yq -i '.framework.cache.pools."pimcore.cache.pool".provider = strenv(rc)' /app/data/pimcore/config/config.yaml


echo '=> Starting Supervisord'
/usr/bin/supervisord
